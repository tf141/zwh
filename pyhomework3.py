'''
文件:    决策树实践.py
时间:    2020/03/11 20:20:37
作者:    20192405 张纹豪
'''
while True:  # 构建循环，最后要使用break退出
    result = input("Continue to buy this game?(yes or no)")   
    if result in ["yes", "y"]:                                # 判断是否购买游戏
        money = int(input("Continue.\nHow much will the game needs?($)"))   
        if money <= 200: # 判断资金是否达到预期
            friend = input("Continue.\nWill  you have enough freinds to play with you?(yes or no)")
            if friend in ["yes", "y"]:                                                               # 判断是否有好友一起玩     
                equmient = input("Continue.\nWill your computer can run it influently?(yes or no)")           
                if equmient in ["yes", "y"]:                                                              # 判断能否流畅运行游戏
                    print(
                        "\033[32mAll needs are met!\nbuy it now!\033[0m")
                elif equmient in ["no", "n"]:
                    print(
                        "\033[31myour computer can not run it influently!\nforget it!\033[0m")
                else:
                    print("\033[31merror!\nplease input yes or no!\033[0m")                             # 特殊情况处理
            elif friend in ["no", "n"]:
                print(
                    "\033[31mNo enough friends can play with you!\ntry other games!\033[0m")
            else:
                print("\033[31merror!\nplease input yes or no!\033[0m")                                 # 特殊情况处理
        else:
            print("\033[31mNeed more golds.\nyou can not offer it!\033[0m")

    elif result in ["no", "n"]:                                                                         # 程序退出
        break
    else:
        print("\033[31merror!\nplease input yes or no!\033[0m")                                         # 特殊情况处理

