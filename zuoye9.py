import sqlite3
conn = sqlite3.connect("20192405.db")
cursor = conn.cursor()
cursor.execute('create table if not exists student (number int(10) primary key, name varchar (20),score float )')
cursor.execute('insert into student(number,name,score) values (20192405,"张纹豪",99.0)')      #插入
cursor.execute('insert into student(number,name,score) values (20192305,"张纹豪",88.0)')
cursor.execute('select * from student')  # 查询
print(cursor.fetchall())  # 输出
cursor.execute('update student set score = 99.0 where number = 20192305')   #修改
cursor.execute('select * from student')  # 查询
print(cursor.fetchall())  # 输出
cursor.execute('delete from student where number = 20192305')     #删除
cursor.execute('select * from student')  # 查询
print(cursor.fetchall())  # 输出
conn.close()

