def qifei(yue,ri):
    riqi = (21, 20, 21, 21, 22, 22, 23, 24, 24, 24, 23, 22)
    xingzuo = ("摩羯座", "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座",
                      "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座")
                     
    # 判断日期是否在指定日期之前，并输出相应的星座
    if ri < riqi[yue-1]:
        return xingzuo[yue-1]      # 在指定日期之前，返回上个月的标号对应的星座
    else:
        return xingzuo[yue]  # 在指定日期之后，返回本月对应的星座
print("判断输入月份对应的星座")
yue = int(input("请输入月份:"))
ri = int(input("请输入日期:"))       
print("{}月{}日的星座为：{}".format(yue, ri, qifei(yue, ri)))
