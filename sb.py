def constellation(month, date):
    '''
    判断输入日期对应的星座
    month:输入的月份
    date:输入的日期
    '''
    dates = (21, 20, 21, 21, 22, 22, 23, 24, 24, 24, 23, 22)
    constellations = ("摩羯座", "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座",
                      "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "摩羯座")
    # 判断日期是否在指定日期之前，并输出相应的星座
    if date < dates[month-1]:
        return constellations[month-1]      # 在指定日期之前，返回上个月的标号对应的星座
    else:
        return constellations[month]  # 在指定日期之后，返回本月对应的星座


def judge(month, date):
    '''
    判断输入的日期是否合理，默认考虑2月29日
    month:输入的月份
    date:输入的日期
    '''
    if month == 2:                           # 若为二月，则date应在1--29之间
        if date in range(1, 30):
            return True
        else:
            return False
    elif month in [1, 3, 5, 7, 8, 10, 12]:  #若为大月，则date应在1--31之间
        if date in range(1, 32):
            return True
        else:
            return False
    elif month in [4, 6, 9, 11]:            #若为小月，则date应在1--30之间
        if date in range(1, 31):
            return True
        else:
            return False


print("判断输入月份对应的星座")
month = int(input("请输入月份:"))
date = int(input("请输入日期:"))
if judge(month,date):
    print("{}月{}日的星座为：{}".format(month, date, constellation(month, date)))
else:
    print("\033[31m请输入正确的日期！\033[0m")