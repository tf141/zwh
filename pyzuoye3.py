#创建列表
tank = ["yase" ,"xiangyu","lvbu","zhangfei","sulie"]
assassin = ["houzi" ,"ake","gongbengwuzang","yunzhongjun","dianwei"]
fashi =["ganjiangmoye" ,"daji","gaojianli" ,"yingzheng"]
#遍历所有列表
for hero in tank:
    print(hero)
for hero1 in assassin:
    print(hero1)
for hero2 in fashi:
    print(hero2)
#修改元素
print(fashi)
fashi[0] = "xishi"
print(fashi)
#增添元素
print(assassin)
assassin.insert(1,"lanlingwang")
print(assassin)
#删除元素
print(fashi)
del fashi[1]
print(fashi)



bilibili = [('异度入侵',2626901),
            ('diancipaoT',314928),
            ('魔法少女',221324),
            ('入间',371314),
            ('taipatong',871126),
            ('花子',344266),
            ('虚构推理',868724),
            ('黑色',583843),
            ('fgo',1564108),
            ('re0',249713)]
del bilibili[:1]          #用切片删除一个元素
bilibili.append( ('异度入侵',2626901))         #添加一个元素
bilibili[1]='电磁炮T'
print("\nbilibili赛高")
bilibili = sorted(bilibili,key = lambda bilibili:bilibili[1],reverse = True)
for idex,item in enumerate(bilibili):
    print(idex+1,"  番名：",item[0],"\n    番评分：",item[1])
